package com.dayup.seckil.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Exceptionhandler {
	
	@ExceptionHandler
	public Object errorHandler(HttpServletRequest request, HttpServletResponse response, Exception e) throws Exception{
		e.printStackTrace();
		if(isJson(request)) {
			return ErrorInfo.ERROR;
		}else {
			ModelAndView mv = new ModelAndView();
			mv.addObject("exception",e);
			mv.addObject("url",request.getRequestURL());
			mv.setViewName("error");
			return mv;
		}
		
	}
	
	public static boolean isJson(HttpServletRequest request) {
		return (request.getHeader("X-Requested-With") != null && "XMLHttpRequest".equals(request.getHeader("X-Requested-With").toString()));
	}
}
