package com.dayup.seckil.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dayup.seckil.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String>{

}
