package com.dayup.seckil.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name="user")
public class User implements Serializable{

	private static final long serialVersionUID = 8271820912883756996L;
	
	public User() {
	}
	
	@Id
	@Column(name="username")
	@NotBlank(message = "username不可以为空！")
	private String username;
	
	@Column(name="password")
	@Size(min=4, max=50, message = "password长度应在4到6之间！")
	private String password;
	
	@Column(name="id")
	private Integer id;
	
	private String repassword;
	
	@Column(name="dbflag")
	private String dbflag;
	
	public String getDbflag() {
		return dbflag;
	}
	public void setDbflag(String dbflag) {
		this.dbflag = dbflag;
	}
	public String getRepassword() {
		return repassword;
	}
	public void setRepassword(String repassword) {
		this.repassword = repassword;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "User [username=" + username + ", password=" + password + ", id=" + id + "]";
	}
	
	public User(String username, String password, Integer id) {
		this.username = username;
		this.password = password;
		this.id = id;
	}
}
