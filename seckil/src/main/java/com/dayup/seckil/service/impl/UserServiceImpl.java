package com.dayup.seckil.service.impl;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dayup.seckil.model.User;
import com.dayup.seckil.repository.UserRepository;
import com.dayup.seckil.service.UserService;


@Service
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User regist(User user) {
		return userRepository.saveAndFlush(user);
	}
	
	
}
