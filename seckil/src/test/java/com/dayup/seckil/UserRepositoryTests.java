package com.dayup.seckil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.dayup.seckil.model.User;
import com.dayup.seckil.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTests {

	@Autowired
	private UserService userService;
	
	
	@Test
	public void testRegistory() {
//		String username = "alex201805";
//		String password = "1234";
//		User user = new User();
//		user.setUsername(username);
//		user.setPassword(password);
//		user.setId(201805);
		//User user = save(username, password);
		//Assert.assertNotNull(user);
		Assert.assertNotNull(userService.regist(new User("alex201805018", "1234", 201805)));
	}

}
